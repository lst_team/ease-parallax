import onScroll from './lib/on-scroll'
import onResize from './lib/on-resize'
import isVisible from './lib/is-visible'
import position from './lib/position'
import EventInterface from './lib/event.interface'
import PositionsInterface from './lib/positions.interface'


export default class EaseParallax {


  constructor(element: string|HTMLElement) {

    if(this.isArgumentConform(element)) {
      const elements: Array<HTMLElement> = this.retrieveElements(element)
      elements.forEach((element: HTMLElement) => {
        this.attachScrollEventWithElement(element)
        this.attachResizeEventWithElement(element)
      })
    }
  }


  private isArgumentConform(element: string|HTMLElement): boolean {
    const isOk = element !== undefined && (typeof element === 'string' || typeof element === 'object')
    console.assert(isOk, '[ease-parallax] You must provide the DOM elements or a selector when instanciating EaseParallax')
    return isOk
  }


  private retrieveElements(selectorOrElement: string|HTMLElement): Array<HTMLElement> {
    const arrayOfElements: Array<HTMLElement> = []

    if(typeof selectorOrElement === 'string') {
      document.querySelectorAll(selectorOrElement).forEach((e: Element) => arrayOfElements.push(<HTMLElement>e))
      return arrayOfElements
    }

    arrayOfElements.push(selectorOrElement)
    return arrayOfElements
  }


  private attachScrollEventWithElement(element: HTMLElement): void {
    onScroll((e: EventInterface) => this.positionElementWhenItsParentIsVisible(element, e))
  }


  private attachResizeEventWithElement(element: HTMLElement) {
    onResize((e: EventInterface) => this.positionElementWhenItsParentIsVisible(element, e))
  }


  private positionElementWhenItsParentIsVisible(element: HTMLElement, scrollEvent: EventInterface): void {
    if(isVisible(<HTMLElement>element.parentNode)) {
      this.positionElementTo(element, position(element, scrollEvent/*, this.trace*/))
    }
  }


  private positionElementTo(element: HTMLElement, position: number) {
    element.style.transform = `translate3d(0, ${position}px, -0)`
    element.style.transition = `transform 0.2s cubic-bezier(0.30, 1, 0.75, 1) 0s`
  }


  private trace(element: HTMLElement, positions: PositionsInterface, newPosition: number) {
    console.log(element, positions, newPosition)
  }


}
