import EventInterface from './event.interface'

export default function eventCore(event: string, fn?: Function): void {

  let ticking: boolean = false

  const eventFn: Function = function() {
    if (!ticking) {
      // Reference: http://www.html5rocks.com/en/tutorials/speed/animations/
      // Use requestAnimationFrame to sync scroll
      // with the repaint of the browser
      window.requestAnimationFrame(() => {
        const me = !this ? {} : this
        // get direction
        const direction: boolean = me.oldScroll > me.scrollY ? false : true
        me.oldScroll = me.scrollY;

        const target = document.body
        const height: number = target.clientHeight
        const top: number =  Math.abs(target.getBoundingClientRect().top)
        const bottom: number =  Math.abs(Math.round(target.getBoundingClientRect().top + target.getBoundingClientRect().height - height))

        if(typeof fn === 'function') {
          const scrollDetails: EventInterface = { direction, top, bottom, height }
          fn(scrollDetails)
        }

        ticking = false;
      });

      ticking = true;
    }
  }

  window.addEventListener(event, () => eventFn())
  // And perform it now
  eventFn()
}
