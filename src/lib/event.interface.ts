export default interface EventInterface {
  direction: boolean,
  top: number,
  bottom: number,
  height: number,
}
