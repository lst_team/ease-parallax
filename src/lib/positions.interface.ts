export default interface PositionsInterface {
  bodyTop: number,
  elementHeight: number,
  parentHeight: number,
  parentTop: number,
}
