import eventCore from './event.core'

export default function onResize(fn?: Function): void {
  eventCore('resize', fn)
}
