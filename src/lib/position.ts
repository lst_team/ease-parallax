import PositionsInterface from './positions.interface'
import EventInterface from './event.interface'

export default function position(element: HTMLElement, scrollEvent: EventInterface, fn?: Function): number {

  const getPositions = function(element: HTMLElement): PositionsInterface {
    // Sometimes body.scrollTop returns 0 when documentElement.scrollTop
    // return the properly value
    const _topToUse: number = (document.body.scrollTop === 0) ? document.documentElement.scrollTop : document.body.scrollTop

    const elementParent: HTMLElement = <HTMLElement>element.parentNode
    const bodyTop: number = Math.floor(_topToUse),
      elementHeight: number = Math.floor(element.getBoundingClientRect().height),
      parentHeight: number = Math.floor(elementParent.getBoundingClientRect().height),
      parentTop: number = Math.floor(elementParent.getBoundingClientRect().top)

    return { bodyTop, elementHeight, parentHeight, parentTop }
  }

  const positions: PositionsInterface = getPositions(element)


  const elementAndParentHeightDiff: number = positions.elementHeight - positions.parentHeight
  const parentHeightAccordingToBodyTop: number = scrollEvent.top * (positions.parentHeight + (positions.parentTop + positions.bodyTop))
  const newPosition: number = -scrollEvent.top * (scrollEvent.top * (elementAndParentHeightDiff / parentHeightAccordingToBodyTop))

  // Mostly for debug purpose
  if(fn) {
    fn(positions, newPosition)
  }

  return newPosition
}
