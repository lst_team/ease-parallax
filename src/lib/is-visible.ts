import inViewport from 'in-viewport'

export default function isVisible(element: HTMLElement) {
  return inViewport(element)
}
