"use strict"

const funnel = require('broccoli-funnel')
const mergeDefinitionFiles = require('./broccoli/merge-definition-files')
const merge = require('broccoli-merge-trees')
const typescript = require('broccoli-typescript-compiler').default
const babel = require('broccoli-babel-transpiler')
const concat = require('broccoli-concat')

const Rollup = require('broccoli-rollup')
const resolve = require('rollup-plugin-node-resolve')
const commonjs = require('rollup-plugin-commonjs')

const LiveReload = require('broccoli-livereload')
const log = require('broccoli-stew').log


const ROOT_PATH = 'src'

// First, get the index.html
let htmlTree = funnel(ROOT_PATH, {
  files: ['index.html'],
  annotation: "[HTML] Transform the index",
});

// Second, get all Typescript files
let tsTree = funnel(ROOT_PATH, {
  destDir: ROOT_PATH,
  exclude: ['**/node_modules/**']
})

// Compile the Typescript files
let jsTree = typescript(tsTree)

// and manually merge the typescript's definition
jsTree = mergeDefinitionFiles(tsTree, jsTree);

// Third, transpile
jsTree = babel(jsTree, {
  annotation: 'broccoli-babel-transpiler',
  plugins: [
    "@babel/plugin-proposal-class-properties",
    "@babel/plugin-proposal-object-rest-spread",
    "@babel/plugin-proposal-private-methods"
  ],
})

// and bundle
jsTree = new Rollup(jsTree, {
  inputFiles: ['ease-parallax.js'],
  annotation: 'broccoli-rollup',
  rollup: {
    input: 'ease-parallax.js',
    output: {
      file: 'ease-parallax.js',
      format: 'umd',
      sourcemap: true,
      name: 'EaseParallax',
    },
    plugins: [
      resolve(),
      commonjs(),
    ]
  }
})


// Finnaly, merge all trees
let tree = merge([htmlTree, jsTree], {
  annotation: "Merge trees",
  overwrite: true,
})


tree = log(tree, { output: 'tree', })
tree = new LiveReload(tree, { target: 'index.html', })



module.exports = tree
