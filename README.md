# ease-parallax

Attach parallax to any element of your page.


## How to use

1. import ease-parallax


``` import EaseParallax from 'ease-parallax' ```

Or

``` <script type="text/javascript" src="/lib/parallax.js"></script> ```



2. Instanciate EaseParallax

```
import EaseParallax from 'ease-parallax'

const myElement = document.getElementById('my-element')
const parallaxOnMyElement = new EaseParallax(myElement)

parallaxOnMyElement.start()
```
